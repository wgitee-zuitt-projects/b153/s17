/*
 		ARRAYS AND INDICES

 		Arrays are used to store multiple related values in a single variable.

 		Arrays are decalred using square brackets [], also known as Array Literals.

 		Values inside of an array are called "elements." Elements inside an array can be changed at any point in time. 

		Index is an element's position inside an array.

*/		

let grades = [98.5, 94.3, 89.2, 90.1]

grades[3] = 80.1;// changing the grade of the last elements - index[3]



console.log(grades.length)

grades[4] = 99.7;// adding a new number at the end of the grades array

grades[grades.length] = 88.8; // We use .length when we are not sure the length of the array. This is the recommended way to add a new value to an array. 


//console.log(grades)

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']


let singleElement = [1]

let emptyArr = []

let example1 = ['cat', 'cat', 'cat']

console.log(computerBrands[3]);// gets the elements in index [3] = 'Neo'.

/*

	READING FROM/ ACCESSING ELEMENTS INSIDE OS AN ARRAY

	To access an array elemnts, simply denote the elements's index number inside a pair of square brackets next to the name/variable of the array holding it.

	To get an element's index, simply count starting at zero from the first element of an array.

	console.log(computerBrands[3])



	GETTING AN ELEMENT'S INDEX

	-- 	We use the keyword "indexOf()" -> e.x. console.log(grades.indexOf(89.2))

	-- If index.Of cannot find your specified elements inside of an array, it returns -1

	E.x.


	if(emails.indexOf("will@email.com") === -1){
		// user can register, because will@email.com does not exist inside the email array (indexOf returned -1)
	}

*/
console.log(grades.indexOf(89.2));


/*
	ARRAY METHODS:
	(Method is just another word for a function)


*/

// 1: .push() - adds an element at the end of an array AND returns the array's length.

// Example: - grades.push(88.3) // adding a new element(88.3) at the end of the grades array

console.log(grades.push(88.3))


// 2: ,pop() - removes the LAST elements in an ARRAY, and returns the removed element

//grades.pop() // removes 88.3 as it is the last element in the array in this case

// console.log(grades) 

//console.log(grades.pop())

grades.push(grades.pop()) // nothing changes in this case

console.log(grades)


let classA = ['John', 'Jacob', 'Jack']

let classB = ['Bob', 'Bill', 'Ben']

classB.push(classA.pop()) // removes "Jack" from classA and adds him to classB.

console.log(classB) // Output: ['Bob', 'Bill', 'Ben', 'Jack']


// 3: .unshift() - Adds an element to the beginning of an array and returns the array's new length

grades.unshift(77.7)

console.log(grades)


// 4: .shift() - removes an array's FIRST element AND returns the removed element

grades.shift()

console.log(grades)


// 5: .splice() - Can add and remove from anywhere in an array, and even do both at once
		// Syntax: .splice(starting index number, number of items to remove, items to add <optional>)



let tasks = [
	'shower',
	'eat breakfast',
	'go to work',
	'go home',
	'go to sleep'
]

//ADD WITH SPLICE
tasks.splice(3, 0, "eat lunch")

//REMOVE WITH SPLICE
tasks.splice(2, 1) // removes the element in index 2

/*	Output
		
		0: "shower"
		1: "eat breakfast"
		2: "eat lunch"
		3: "go home"
		4: "go to sleep"

*/


// ADD AND REMOVE WITH SPLICE
tasks.splice(3, 1, "code") // removed the element "go home" in index 3 and added "code" to index 3

/*	Output
		
		0: "shower"
		1: "eat breakfast"
		2: "eat lunch"
		3: "code"
		4: "go to sleep"

*/

//console.log(tasks)


// 6: .sort() - rearranges array elements in alphanumeric order

computerBrands.sort()

/*  Output

		0: "Acer"
		1: "Asus"
		2: "Fujitsu"
		3: "Gateway"
		4: "Lenovo"
		5: "Neo"
		6: "Redfox"
		7: "Toshiba"
*/

//console.log(computerBrands)


// 7: .reverse() - reverses the order of your array elements

computerBrands.reverse()

/*	Output
		
		0: "Toshiba"
		1: "Redfox"
		2: "Neo"
		3: "Lenovo"
		4: "Gateway"
		5: "Fujitsu"
		6: "Asus"
		7: "Acer"
*/

//console.log(computerBrands)


// 8: .concat() - comnines two arrays into one

let a = [1, 2, 3, 4, 5]
let b = [6, 7, 8, 9, 10]

let ab = a.concat(b)

/*	Output

		0: 1
		1: 2
		2: 3
		3: 4
		4: 5
		5: 6
		6: 7
		7: 8
		8: 9
		9: 10
*/

//console.log(ab)


// 9: .join() - converts the elements of an array into a new string (without the square bracket)

let joinedArray = ab.join()

/*	Output

		1,2,3,4,5,6,7,8,9,10
*/

// console	.log(joinedArray)


// If you need to compare the elements exactly between arrays, .join() can be used to get the accurate comparison 

let arr1 = [1, 2, 3, 4, 5];
let arr2 = [1, 2, 3, 4, 5];
console.log(arr1 === arr2); // false
console.log(arr1.join() === arr2.join()); // true - because the .join method changes the two arrays into strings first before checking if they are similar, thus it returns "true"


// 10: .slice() - copies specified range array elements into a new array

//Syntax: - slice(starting array, stopping point) - the stopping point is not copied


// EVERYTHING before the stopping point is copied

let c = a.slice(0, 3)

/*	Output

		0: 1
		1: 2
		2: 3
*/

console.log(a)
console.log(c)


//	ARRAY ITERATION METHODS
	//JavaScript Loops only for arrays
	// Always loope through the exact number of times as there are elements in the array


// 1. .forEach()

computerBrands.forEach(function(brand){
	console.log(brand)
})
	/* Works exactly the same as:

		for(let i = 0; i < computerBrands.length; i++){
			console.log(ComputerBrands[i])
		}

	*/

// 2. .map()

let numbersArr = [1, 2, 3, 4, 5]

let doubledNumbers = numbersArr.map(function(number){
	return number * 2
})

console.log(doubledNumbers)

// 3. .every() - check every element in an array and see if they ALL match your given condition// returns false since 3 !< 3

let allValid = numbersArr.every(function(number){
	//return number < 3;

	return (typeof number === 'number') // returns true
})

console.log(allValid)


// 4. .some() - checks every element in an array and see if SOME match your given condition

let someValid = numbersArr.some(function(number) {
	// body...
	return number < 3
})

console.log(someValid) // true

// 5. .filter() - creates a new array only populated with elements that match our digen condition

let words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present']

let wordsResult = words.filter(function(word){
	return word.length > 6
})

console.log(wordsResult) // results ['exuberant', 'destruction', 'present'] - because these words have a length more than 6 characters, as it is supposed to return words with a length greater then 6


// 6. .includes() - checks if the specified value is included in the array (returns true or false)

console.log(words.includes('limit'))


// TWO DIMENSIONAL ARRAYS

let twoDimensional = [
	[1.1, 1.2, 1.3],
	[2.1, 2.2, 2.3],
	[3.1, 3.2, 3.3]
]

//console.log(twoDimensional[0][1]) // gives a result of 1.2 as it has an index of 1

console.log(twoDimensional[2][2][3]) // gives a result of 3.3