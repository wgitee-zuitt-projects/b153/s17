let students = []

function addStudent(firstName){
	students.push(firstName)
	console.log(firstName + " was added to the students list.")
}

function countStudents(){
	console.log("There are a total of " + students.length + " students enrolled.")
}

function printStudents(){
	
	let lowerCaseNames = students.map(function(student){
		return student.toLowerCase()
	})

	students.sort()

	students.forEach(function(student){
		console.log(student)
	})
}

function findStudents(name){
	name = name.toLowerCase()

	let lowerCaseNames = students.map(function(student){
		return student.toLowerCase()
	})
	console.log(lowerCaseNames)

	if(lowerCaseNames.includes(name)){
		console.log(name + " is an enrollee.")
	}else{
		console.log("No student was found with the name " +name)
	}

	let result = lowerCaseNames.filter((letter) => letter.startsWith("j"))
	console.log(result + " are enrollees")
}

function addSection(section){
	let studentSections = students.map(function(student){
		return student + ' - Section ' + section
	})
	console.log(studentSections)
}

function removeStudent(student){
	student = student.toLowerCase()

	let lowerCaseNames = students.map(function(student){
		return student.toLowerCase()
	})
	let studentIndex = lowerCaseNames.studentIndex(student)
	lowerCaseNames.splice(studentIndex, 1)

	console.log(student + " was removed from the student's list.")

	students = lowerCaseNames

	console.log(lowerCaseNames)
}